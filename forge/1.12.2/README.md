## Instuctions for installing this modpack

1. Create a new forge vanilla instance for MC 1.12.2
1. Copy the instance.json file into the instance's folder
1. Go get the IR resource packs and put them in the config/immersiverailroading folder on the instance
   - American Caboos pack 2.11
   - American Freight Pack
   - American Steam Locomotive Pack
   - Amtrack+
   - DarkRaider's Freight


## Servers

gaming.blackhawkelectronics.com:1435
